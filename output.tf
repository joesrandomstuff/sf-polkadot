output "instance_name" {
  value = aws_instance.ubuntu[*].tags.Name
}

output "instance_public_ip" {
  value = aws_instance.ubuntu[*].public_ip
}

output "instance_public_dns" {
  value = aws_instance.ubuntu[*].public_dns
}
