terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

resource "aws_instance" "ubuntu" {
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_pair_name
  vpc_security_group_ids = ["${aws_security_group.ingress-all-test.id}"]
  count                  = 2

  credit_specification {
    cpu_credits = "unlimited"
  }

  tags = {
    Name = "sf-node${count.index}"
  }
}

provider "aws" {
  shared_config_files      = ["/home/joe/.aws/config"]
  shared_credentials_files = ["/home/joe/.aws/credentials"]
  region                   = var.region
}
