variable "ami" {
  type = string
  default = "ami-0fe8bec493a81c7da"
}

variable "instance_type" {
  type = string
  default = "t3.micro"
}

variable "region" {
  type = string
  default = "eu-north-1"
}

variable "key_pair_name" {
  type = string
  default = "mydummy-01"
}
