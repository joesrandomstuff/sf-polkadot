# Work sample for Staking Facilities

# Intro
This is a work sample from me for Staking Facilities according to a given assignment.
The assignment asks for:
* A setup of two vm instances with any provider
* Deployment of a specific polkadot version with an upgrade path by ansible playbook
* Ensuring that polkadot is run (utilizing systemd)
* Detailing the update process

# Scope
To keep the time and complexity within reasonable boundaries I had to decide on some sacrifices. The terraform setup and ansible playbook work within my understanding of the assignment but I see a lot of open work with this to be safely and efficiently usable.

# Choices
This section details some choices I made.

* Debian Package

  If available and done in a sane way, I would always prefer using the native package for the used distribution. This comes with multiple benefits: for one (still, depending on distro a little), the security is handled. The package checksum is validated and can not be compromised in an easy way. On the other hand, this usually comes with appropriate systemd units and other auxiliary files maintained by the developer, lessening the effort on the operations side. Last but not least up and downgrading (the package itself, not mentioning surrounding issues) is usually a trivial matter with this as the distribution tools provide this.

* AWS

  I simply chose aws because it has all services and also a free tier.

# Missing
This section is a collection of critical things I see missing.
* User management (OS users), SSH Key mangement
* Bare metal tuning
* Hardening
* Monitoring (already running exporter needs scraping; dashboards & alertrules)
* Polkadot configuration

  Likely there is something if not even a lot missing there. Since the assignment didn't specify this and I don't know polkadot at all yet there is currently nothing. However, this would be likely fairly easy to add to the playbook (template + handler for service restart on change).

# Recommendations
This section is a collection of things I recommend.
* instance_type c6i.4xlarge

  This is a given of course with the official documentation stating that this instance type should be used.
* gitlab-ci pipelines

  It would be a great benefit for this to utilize gitlab-ci pipelines. These could completely move the usage to pipelines, also with variables (polkadot version) and manual triggers where required.

# Prerequisites
* aws cli needs to be set up with credentials and working.

  The credentials are referenced in main.tf, shared_config_files and shared_credentials_files.
* A key pair needs to be configured in aws already and inserted into the terraform configuration as well as being used by the ssh that the playbook will be run on.

  See variables.tf, key_pair_name.
* The instance_type **should** be c6i.4xlarge for production, but is t3.micro in my example.

# Usage
## Terraform setup of the two instances
The regular usage of terraform applies:
```
terraform init
terraform plan
terraform apply
```
Now there will be two instances running in aws, called sf-node0 and sf-node1, provisioned with the latest ubuntu LTS and reachable by ssh.

The output will provide some information of the created instances. Copy the public IPs or DNS names into the ansible inventory called inventory.

## Ansible polkadot provisioning and update
Run the playbook as follows:
```
ansible-playbook site.yml -i inventory --extra-vars '{"polkadot_version":"1.2.0"}'
```
After running the playbook polkadot will be installed in the given version and running. An upgrade is done the same way.

